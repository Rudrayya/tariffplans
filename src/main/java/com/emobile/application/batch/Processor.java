package com.emobile.application.batch;

import java.util.Optional;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.emobile.application.entity.Traifplans;
import com.emobile.application.repository.TraifplansRepository;

public class Processor implements ItemProcessor<Traifplans,Traifplans>
{
	
	@Autowired
	private TraifplansRepository traifRepo;

	@Override
	public Traifplans process(Traifplans traif) throws Exception {
		Optional<Traifplans> traifFromDb = traifRepo.findById(traif.getPlanId());
		if(traifFromDb.isPresent()) {
			traif.setPlanId(traif.getPlanId());
		}
		return traif;
	}

}
